package assign.services;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import assign.domain.*;
import java.util.logging.*;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

public class DBloader {
	private SessionFactory sessionFactory;
	
	Logger logger;
	
	public DBloader() throws MalformedURLException, IOException {
		// A SessionFactory is set up once for an application
        sessionFactory = new Configuration()
                .configure() // configures settings from hibernate.cfg.xml
                .buildSessionFactory();
        
        //urls to Extract data from
        String url1 = "http://www.cs.utexas.edu/~devdatta/traffic_incident_data.xml";
        String url2 = "http://www.cs.utexas.edu/~devdatta/traffic_incident_with_zipcodes.xml";
        
        loadData(url1, url2);
        logger = Logger.getLogger("DBLoader");
	}
	
	//allow other classes to access the session factory
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	public void loadData(String url1, String url2) throws MalformedURLException, IOException {
		//logger.info("Inside loadData.");
		NodeList nodeList1 = parseXML(url1);
		NodeList nodeList2 = parseXML(url2);
		
		List<IncidentUnionLoad> mergedReports = transformData(nodeList1, nodeList2);
		loadReports(mergedReports);
	}
	

	private List<IncidentUnionLoad> transformData(NodeList nodeList1, NodeList nodeList2) {
		
		ArrayList<IncidentUnionLoad> listOfReports = new ArrayList<IncidentUnionLoad>();
		HashMap<String, String> addToZip = new HashMap<String, String>();
		
		
		//populate the hashmap for address to zipcode
		NodeList nl2 = nodeList2.item(0).getFirstChild().getChildNodes();
		for (int i =0; i < nl2.getLength(); i++) {
			NodeList current = nl2.item(i).getChildNodes();
			String add = "";
			String zip = "";
			for (int j = 0; j < current.getLength(); j++) {
				if (current.item(j).getNodeName().equals("ds:address")) {
					add = current.item(j).getTextContent();
				}
				else if (current.item(j).getNodeName().equals("ds:zipcode")) {
					zip = current.item(j).getTextContent();
				}
			}
			//populate the hashmap
			addToZip.put(add, zip);
		}
		
		//put info from url1 into the union object
		NodeList nl1 = nodeList1.item(0).getFirstChild().getChildNodes();
		for (int i = 0; i < nl1.getLength(); i++) {
			NodeList current = nl1.item(i).getChildNodes();
			IncidentUnionLoad temp = new IncidentUnionLoad();
			for(int j = 0; j < current.getLength(); j++) {
				//create new union object to fill	
					if(current.item(j).getNodeName().equals("ds:published_date")) {
						temp.setPublishedDate(current.item(j).getTextContent());
						
					}
					else if(current.item(j).getNodeName().equals("ds:issue_reported")) {
						temp.setIssueReported(current.item(j).getTextContent());
					}
					//get the zip when we find the address
					else if(current.item(j).getNodeName().equals("ds:address")) {
						temp.setAddress(current.item(j).getTextContent());
						temp.setZipcode(addToZip.get(current.item(j).getTextContent()));
					}
			}
			//ask I get a #text node that i have to skip
			if(!temp.getAddress().equals("-1"))
			listOfReports.add(temp);
		}
		
		return listOfReports;
	}
	
	private void loadReports(List<IncidentUnionLoad> mergedReports) {
		System.out.println("WE ARE HERE IN THIS FUNCTION ABOUT TO LOAD STUFF");
		
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		Long assignmentId = null;
		for(IncidentUnionLoad u: mergedReports) {
			try {
				tx = session.beginTransaction();
				session.save(u);
			    assignmentId = u.getId();
			    tx.commit();
			} 
			catch (Exception e) {
				if (tx != null) {
					tx.rollback();
					throw e;
				}
			}
		}
		
		session.close();
	}

	//need xpath parsing stuff
	//stuff for parsing XML
   public NodeList parseXML (String url) throws MalformedURLException, IOException {
		//Parse XML file
	   	
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e2) {
			e2.printStackTrace();
		}
		Document doc = null;
		try {
			//doc = builder.parse(response.getEntity().getContent());
			doc = builder.parse(new URL(url).openStream());
		} catch (SAXException e1) {
			e1.printStackTrace();
		}
		
		//Get XPath expression
		XPath xpath = XPathFactory.newInstance().newXPath();
		xpath.setNamespaceContext(new NamespaceResolver(doc));
		//ds:traffic_report_id
		
		NodeList nodes = null;
		try {
			nodes = (NodeList) xpath.evaluate("/", doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		if (nodes == null) {
			return null;
		}
		else
			return nodes;
	}
		
	private class NamespaceResolver implements NamespaceContext {
	    //Store the source document to search the namespaces
	    private Document sourceDocument;
	 
	    public NamespaceResolver(Document document) {
	        sourceDocument = document;
	    }
	 
	    //The lookup for the namespace uris is delegated to the stored document.
	    public String getNamespaceURI(String prefix) {
	        if (prefix.equals(XMLConstants.DEFAULT_NS_PREFIX)) {
	            return sourceDocument.lookupNamespaceURI(null);
	        } else {
	            return sourceDocument.lookupNamespaceURI(prefix);
	        }
	    }
	 
	    public String getPrefix(String namespaceURI) {
	        return sourceDocument.lookupPrefix(namespaceURI);
	    }
	 
	    @SuppressWarnings("rawtypes")
	    public Iterator getPrefixes(String namespaceURI) {
	        return null;
	    }
	}
}
