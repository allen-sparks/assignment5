package assign.services;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import assign.domain.*;


public class TrafficReportServiceImpl  {
	
	public TrafficReportServiceImpl() {}
	
	public ZipCount getZipCount(String zipcode, DBloader database) {
		Session session = database.getSessionFactory().openSession();
		session.beginTransaction();	
		
		Criteria crit = session.createCriteria(IncidentUnionLoad.class);
		crit.add( Restrictions.eq("zipcode", zipcode));
		crit.setProjection(Projections.rowCount());
		Long count = (Long)crit.uniqueResult();
				
		session.close();
		ZipCount zipCount = new ZipCount(zipcode, count.intValue());
		
		return zipCount;
	}
	
	public DateCount getDateCount(String date, DBloader database) {
		Session session = database.getSessionFactory().openSession();
		session.beginTransaction();	
		
		Criteria crit = session.createCriteria(IncidentUnionLoad.class);
		crit.add( Restrictions.eq("publishedDate", date));
		crit.setProjection(Projections.rowCount());
		Long count = (Long)crit.uniqueResult();
				
		session.close();
		DateCount dateCount = new DateCount(date, count.intValue());
		
		return dateCount;
	}
	
	public IssCount getIssCount(String iss, DBloader database) {
		Session session = database.getSessionFactory().openSession();
		session.beginTransaction();	
		
		Criteria crit = session.createCriteria(IncidentUnionLoad.class);
		crit.add( Restrictions.eq("issueReported", iss));
		crit.setProjection(Projections.rowCount());
		Long count = (Long)crit.uniqueResult();
				
		session.close();
		IssCount issCount = new IssCount(iss, count.intValue());
		
		return issCount;
	}
	
	public AddCount getAddCount(String add, DBloader database) {
		Session session = database.getSessionFactory().openSession();
		session.beginTransaction();	
		
		Criteria crit = session.createCriteria(IncidentUnionLoad.class);
		crit.add( Restrictions.eq("address", add));
		crit.setProjection(Projections.rowCount());
		Long count = (Long)crit.uniqueResult();
				
		session.close();
		AddCount addCount = new AddCount(add, count.intValue());
		
		return addCount;
	}
}