package assign.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table( name = "Incident_Union" )
public class IncidentUnionLoad {
	
	//Unique ID in the database
	private Long id;
	//columns of the table
	private String published_date;
	private String issue_reported;
	private String address;
	private String zipcode;
	
	public IncidentUnionLoad() {
		this.published_date = "-1";
		this.issue_reported = "-1";
		this.address = "-1";
		this.zipcode = "-1";
	}
	
	public IncidentUnionLoad(String pub, String iss, String add, String zip) {
		this.published_date = pub;
		this.issue_reported = iss;
		this.address = add;
		this.zipcode = zip;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	
	private void setId(Long id) {
		this.id = id;
	}
	
	public String getPublishedDate() {
		return this.published_date;
	}
	
	public void setPublishedDate(String date) {
		this.published_date = date;
	}
	
	public String getIssueReported() {
		return issue_reported;
	}
	
	public void setIssueReported(String iss) {
		this.issue_reported = iss;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String add) {
		this.address = add;
	}
	
	public String getZipcode() {
		return zipcode;
	}
	
	public void setZipcode(String zip) {
		this.zipcode = zip;
	}
}
