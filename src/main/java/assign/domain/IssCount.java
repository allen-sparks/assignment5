package assign.domain;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlType (propOrder = {"issue_reported", "count"})
@XmlRootElement(name = "output")
@XmlAccessorType
public class IssCount {
	
	private String issue_reported;
	int count;
	
	public IssCount() {
		//empty constructor
	}
	
	public IssCount(String issue_reported, int count) {
		this.issue_reported = issue_reported;
		this.count = count;
	}

	public String getIssue_reported() {
		return issue_reported;
	}

	public void setIssue_reported(String issue_reported) {
		this.issue_reported = issue_reported;
	}
	
	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}

}
