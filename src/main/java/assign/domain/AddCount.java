package assign.domain;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlType (propOrder = {"address", "count"})
@XmlRootElement(name = "output")
@XmlAccessorType
public class AddCount {
	
	private String address;
	int count;
	
	public AddCount() {
		//empty constructor
	}
	
	public AddCount(String address, int count) {
		this.address = address;
		this.count = count;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}

}
