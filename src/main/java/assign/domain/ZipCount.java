package assign.domain;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlType (propOrder = {"zipcode", "count"})
@XmlRootElement(name = "output")
@XmlAccessorType
public class ZipCount {
	
	private String zipcode;
	int count;
	
	public ZipCount() {
		//empty constructor
	}
	
	public ZipCount(String zip, int count) {
		this.zipcode = zip;
		this.count = count;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zip) {
		this.zipcode = zip;
	}
	
	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}

}
