package assign.domain;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlType (propOrder = {"published_date", "count"})
@XmlRootElement(name = "output")
@XmlAccessorType
public class DateCount {
	
	private String published_date;
	int count;
	
	public DateCount() {
		//empty constructor
	}
	
	public DateCount(String zip, int count) {
		this.published_date = zip;
		this.count = count;
	}

	public String getPublished_date() {
		return published_date;
	}

	public void setPublished_date(String date) {
		this.published_date = date;
	}
	
	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}

}
