package assign.resources;

import java.io.IOException;
import java.net.MalformedURLException;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import assign.domain.*;
import assign.services.*;

@Path("/trafficincidentreports")
public class TrafficReportResource {
	
	private static DBloader database;
	TrafficReportServiceImpl trs;

	
	public TrafficReportResource(@Context ServletContext servletContext) throws MalformedURLException, IOException {
		if (database == null) {
			database = new DBloader();
		}

		this.trs = new TrafficReportServiceImpl();		
	}
	
	@GET
	@Path("/")
	@Produces("text/plain")
	public String landingPage() {
		return "Welcome to Traffic Incident Reports REST API.\n" + 
				"Following queries are supported by this REST API.\n\n" +
				"For zipcodes: http://localhost:8080/assignment5/trafficincidentreports/reports/zipcode/<zipcode>\n"
				+ "For published_date: http://localhost:8080/assignment5/trafficincidentreports/reports/published_date/<published_date>\n"
				+ "For issue_reported: http://localhost:8080/assignment5/trafficincidentreports/reports/issue_reported/<issue_reported>\n"
				+ "For address: http://localhost:8080/assignment5/trafficincidentreports/reports/address/<address>";
	}
	
	@GET
	@Path("/zipcode/{zipcode}")
	//@Produces("applicaiton/html") why does removing this work?
	public ZipCount zipCount(@PathParam("zipcode") String zipcode) {
		
		return trs.getZipCount(zipcode, database);		
	}
	
	@GET
	@Path("/published_date/{published_date}")
	public DateCount dateCount(@PathParam("published_date") String date) {
		
		return trs.getDateCount(date, database);		
	}
	
	@GET
	@Path("/issue_reported/{issue_reported}")
	public IssCount issCount(@PathParam("issue_reported") String iss) {
		iss = iss.replace("%20", " ");
		return trs.getIssCount(iss, database);		
	}
	
	@GET
	@Path("/address/{address}")
	public AddCount addCount(@PathParam("address") String add) {
		add = add.replace("%20", " ");
		return trs.getAddCount(add, database);		
	}
	
}