package assign.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Before;
import org.junit.Test;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import javax.xml.namespace.NamespaceContext;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import java.net.MalformedURLException;
import java.util.Iterator;


public class TestTrafficIncidentReportsResource
{

   private HttpClient client;

   @Before
   public void initClient()
   {
	   client = HttpClientBuilder.create().build();
   }

   @Test
   public void testPostReport() throws Exception
   {
      String body = "<report>"
      		+ "<published_date>0</published_date>"
      		+ "<issue_reported>Crash Service</issue_reported>"
      		+ "<address>W 21ST ST and GUADALUPE ST</address>"
      		+ "<zipcode>78717</zipcode>"
      		+ "</report>";
      
      String url = "http://localhost:8080/assignment4/trafficincidentreports/reports/";
      
      // Code snippet taken from https://www.mkyong.com/java/apache-httpclient-examples/
      HttpPost request = new HttpPost(url);
      HttpEntity ent = new ByteArrayEntity(body.getBytes("UTF-8"));
      
      request.setHeader(HttpHeaders.CONTENT_TYPE, "application/xml");
      request.setEntity(ent);
      
      HttpResponse response = client.execute(request);
      NodeList responseBody = parseXML(response);
      
      String idNumber = null;
      
      NodeList toIterate = responseBody.item(0).getChildNodes().item(0)
    		  .getChildNodes();
      for(int i = 0; i < toIterate.getLength(); i++) {
    	  Node currentNode = toIterate.item(i);
    	  if(currentNode.getNodeName().equals("id")) {
    		  idNumber = currentNode.getTextContent();
    	  }
      }
      
      assertNotNull(idNumber);
      assertNotEquals(idNumber, "");
      
      String urlDelete = "http://localhost:8080/assignment4/trafficincidentreports"
      		+ "/reports/" + idNumber;
      HttpDelete deleteTest = new HttpDelete(urlDelete);
      HttpResponse dResponse = client.execute(deleteTest);
   }   
   
   @Test
   public void testPutReport() throws Exception {
	   String body = "<report>"
	      		+ "<published_date>0</published_date>"
	      		+ "<issue_reported>Crash Service</issue_reported>"
	      		+ "<address>W 21ST ST and GUADALUPE ST</address>"
	      		+ "<zipcode>78717</zipcode>"
	      		+ "</report>";
	      
	      String url = "http://localhost:8080/assignment4/trafficincidentreports/reports/";
	      
	      // Code snippet taken from https://www.mkyong.com/java/apache-httpclient-examples/
	      HttpPost request = new HttpPost(url);
	      HttpEntity ent = new ByteArrayEntity(body.getBytes("UTF-8"));
	      
	      request.setHeader(HttpHeaders.CONTENT_TYPE, "application/xml");
	      request.setEntity(ent);
	      
	      HttpResponse response = client.execute(request);
	      NodeList responseBody = parseXML(response);
	      
	      String idNumber = null;
	      
	      NodeList toIterate = responseBody.item(0).getChildNodes().item(0)
	    		  .getChildNodes();
	      for(int i = 0; i < toIterate.getLength(); i++) {
	    	  Node currentNode = toIterate.item(i);
	    	  if(currentNode.getNodeName().equals("id")) {
	    		  idNumber = currentNode.getTextContent();
	    	  }
	      }
	      
	      String updateBody = "<report>"
		      		+ "<published_date>update</published_date>"
		      		+ "<issue_reported>update</issue_reported>"
		      		+ "<address>update</address>"
		      		+ "<zipcode>update</zipcode>"
		      		+ "</report>";
	      
	      String updateUrl = "http://localhost:8080/assignment4/trafficincidentreports/"
	      		+ "reports/" + idNumber;
	      
	      HttpPut update = new HttpPut(updateUrl);
	      HttpEntity ent2 = new ByteArrayEntity(updateBody.getBytes("UTF-8"));
	      
	      update.setHeader(HttpHeaders.CONTENT_TYPE, "application/xml");
	      update.setEntity(ent2);
	      
	      HttpResponse response2 = client.execute(update);
	      NodeList updateResponse = parseXML(response2);
	      
	      String updateId = null;
	      String published = null;
	      String issue = null;
	      String address = null;
	      String zip = null;
	      
	      NodeList toIterate2 = updateResponse.item(0).getChildNodes().item(0)
	    		  .getChildNodes();
	      for(int i = 0; i < toIterate2.getLength(); i++) {
	    	  Node currentNode = toIterate2.item(i);
	    	  if(currentNode.getNodeName().equals("id")) {
	    		  updateId = currentNode.getTextContent();
	    	  }
	    	  else if (currentNode.getNodeName().equals("published_date")) {
	    		  published = currentNode.getTextContent();
	    	  }
	    	  else if (currentNode.getNodeName().equals("issue_reported")) {
	    		  issue = currentNode.getTextContent();
	    	  }
	    	  else if (currentNode.getNodeName().equals("address")) {
	    		  address = currentNode.getTextContent();
	    	  }
	    	  else if (currentNode.getNodeName().equals("zipcode")) {
	    		  zip = currentNode.getTextContent();
	    	  }
	      }
	      
	      assertEquals(response2.getStatusLine().getStatusCode(), 200, 0);
	      assertEquals(idNumber, updateId);
	      assertEquals(published, "update");
	      assertEquals(issue, "update");
	      assertEquals(address, "update");
	      assertEquals(zip, "update");
	      
	      String urlDelete = "http://localhost:8080/assignment4/trafficincidentreports"
	        		+ "/reports/" + idNumber;
	        HttpDelete deleteTest = new HttpDelete(urlDelete);
	        HttpResponse dResponse = client.execute(deleteTest);
	        assertEquals(dResponse.getStatusLine().getStatusCode(), 200, 0);
   }
   
   @Test
   public void testDeleteReport() throws Exception {
	   String body = "<report>"
	      		+ "<published_date>0</published_date>"
	      		+ "<issue_reported>Crash Service</issue_reported>"
	      		+ "<address>W 21ST ST and GUADALUPE ST</address>"
	      		+ "<zipcode>78717</zipcode>"
	      		+ "</report>";
	      
	      String url = "http://localhost:8080/assignment4/trafficincidentreports/reports/";
	      
	      // Code snippet taken from https://www.mkyong.com/java/apache-httpclient-examples/
	      HttpPost request = new HttpPost(url);
	      HttpEntity ent = new ByteArrayEntity(body.getBytes("UTF-8"));
	      
	      request.setHeader(HttpHeaders.CONTENT_TYPE, "application/xml");
	      request.setEntity(ent);
	      
	      HttpResponse response = client.execute(request);
	      NodeList responseBody = parseXML(response);
	      
	      String idNumber = null;
	      
	      NodeList toIterate = responseBody.item(0).getChildNodes().item(0)
	    		  .getChildNodes();
	      for(int i = 0; i < toIterate.getLength(); i++) {
	    	  Node currentNode = toIterate.item(i);
	    	  if(currentNode.getNodeName().equals("id")) {
	    		  idNumber = currentNode.getTextContent();
	    	  }
	      }
	      
	      String urlDelete1 = "http://localhost:8080/assignment4/trafficincidentreports"
	        		+ "/reports/" + idNumber;
	      HttpDelete delete1 = new HttpDelete(urlDelete1);
	      HttpResponse dResponse = client.execute(delete1);
	      assertEquals(dResponse.getStatusLine().getStatusCode(), 200, 0);
	      
	      String urlDelete2 = "http://localhost:8080/assignment4/trafficincidentreports"
	        		+ "/reports/" + idNumber;
	      HttpDelete delete2 = new HttpDelete(urlDelete2);
	      HttpResponse dResponse2 = client.execute(delete2);
	      assertEquals(dResponse2.getStatusLine().getStatusCode(), 404, 0);
   }
   
   @Test
   public void testGetReport() throws Exception {
	   String body = "<report>"
	      		+ "<published_date>0</published_date>"
	      		+ "<issue_reported>Crash Service</issue_reported>"
	      		+ "<address>W 21ST ST and GUADALUPE ST</address>"
	      		+ "<zipcode>78717</zipcode>"
	      		+ "</report>";
	      
	      String url = "http://localhost:8080/assignment4/trafficincidentreports/reports/";
	      
	      // Code snippet taken from https://www.mkyong.com/java/apache-httpclient-examples/
	      HttpPost request = new HttpPost(url);
	      HttpEntity ent = new ByteArrayEntity(body.getBytes("UTF-8"));
	      
	      request.setHeader(HttpHeaders.CONTENT_TYPE, "application/xml");
	      request.setEntity(ent);
	      
	      HttpResponse response = client.execute(request);
	      NodeList responseBody = parseXML(response);
	      
	      String idNumber = null;
	      
	      NodeList toIterate = responseBody.item(0).getChildNodes().item(0)
	    		  .getChildNodes();
	      for(int i = 0; i < toIterate.getLength(); i++) {
	    	  Node currentNode = toIterate.item(i);
	    	  if(currentNode.getNodeName().equals("id")) {
	    		  idNumber = currentNode.getTextContent();
	    	  }
	      }
	      
	      assertNotNull(idNumber);
	      assertNotEquals(idNumber, "");
	      
	      String urlGet = "http://localhost:8080/assignment4/trafficincidentreports"
	      		+ "/reports/" + idNumber;
	      
	      HttpGet get = new HttpGet(urlGet);
	      HttpResponse getResponse = client.execute(get);
	      NodeList gResponseBody = parseXML(getResponse);
	      
	      String updateId = null;
	      String published = null;
	      String issue = null;
	      String address = null;
	      String zip = null;
	      
	      NodeList toIterate2 = gResponseBody.item(0).getChildNodes().item(0)
	    		  .getChildNodes();
	      for(int i = 0; i < toIterate2.getLength(); i++) {
	    	  Node currentNode = toIterate2.item(i);
	    	  if(currentNode.getNodeName().equals("id")) {
	    		  updateId = currentNode.getTextContent();
	    	  }
	    	  else if (currentNode.getNodeName().equals("published_date")) {
	    		  published = currentNode.getTextContent();
	    	  }
	    	  else if (currentNode.getNodeName().equals("issue_reported")) {
	    		  issue = currentNode.getTextContent();
	    	  }
	    	  else if (currentNode.getNodeName().equals("address")) {
	    		  address = currentNode.getTextContent();
	    	  }
	    	  else if (currentNode.getNodeName().equals("zipcode")) {
	    		  zip = currentNode.getTextContent();
	    	  }
	      }
	      
	      assertEquals(getResponse.getStatusLine().getStatusCode(), 200, 0);
	      assertEquals(idNumber, updateId);
	      assertEquals(published, "0");
	      assertEquals(issue, "Crash Service");
	      assertEquals(address, "W 21ST ST and GUADALUPE ST");
	      assertEquals(zip, "78717");
	      
	      String urlDelete = "http://localhost:8080/assignment4/trafficincidentreports"
	      		+ "/reports/" + idNumber;
	      HttpDelete deleteTest = new HttpDelete(urlDelete);
	      HttpResponse dResponse = client.execute(deleteTest);
	      assertEquals(dResponse.getStatusLine().getStatusCode(), 200, 0);
   }
   
   //stuff for parsing XML
   public NodeList parseXML (HttpResponse response) throws MalformedURLException, IOException {
		//Parse XML file
	   	
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e2) {
			e2.printStackTrace();
		}
		Document doc = null;
		try {
			doc = builder.parse(response.getEntity().getContent());
			//doc = builder.parse(new URL(result).openStream());
		} catch (SAXException e1) {
			e1.printStackTrace();
		}
		
		//Get XPath expression
		XPath xpath = XPathFactory.newInstance().newXPath();
		xpath.setNamespaceContext(new NamespaceResolver(doc));
		//ds:traffic_report_id
		
		NodeList nodes = null;
		try {
			nodes = (NodeList) xpath.evaluate("/", doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		if (nodes == null) {
			return null;
		}
		else
			return nodes;
	}
	
	private class NamespaceResolver implements NamespaceContext {
	    //Store the source document to search the namespaces
	    private Document sourceDocument;
	 
	    public NamespaceResolver(Document document) {
	        sourceDocument = document;
	    }
	 
	    //The lookup for the namespace uris is delegated to the stored document.
	    public String getNamespaceURI(String prefix) {
	        if (prefix.equals(XMLConstants.DEFAULT_NS_PREFIX)) {
	            return sourceDocument.lookupNamespaceURI(null);
	        } else {
	            return sourceDocument.lookupNamespaceURI(prefix);
	        }
	    }
	 
	    public String getPrefix(String namespaceURI) {
	        return sourceDocument.lookupPrefix(namespaceURI);
	    }
	 
	    @SuppressWarnings("rawtypes")
	    public Iterator getPrefixes(String namespaceURI) {
	        return null;
	    }
	}
}