create database crashReports;

use database crashReports;

create table reports (id int not null auto_increment, published_date varchar(255) not null, issue_reported varchar(255) not null, address varchar(255) not null, zipcode varchar(255) not null, primary key(id));

